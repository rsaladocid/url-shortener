package com.rsaladocid.urlshortener.security;

import java.util.Arrays;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.rsaladocid.urlshortener.model.Role;
import com.rsaladocid.urlshortener.model.User;
import com.rsaladocid.urlshortener.repositories.RoleRepository;
import com.rsaladocid.urlshortener.repositories.UserRepository;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	boolean alreadySetup = false;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (alreadySetup) {
			return;
		}

		createRoleIfNotFound(RoleConstants.SUPER_ADMIN);
		createRoleIfNotFound(RoleConstants.ADMIN);
		createRoleIfNotFound(RoleConstants.USER);
		createRoleIfNotFound(RoleConstants.GUEST);

		createAdminUser();
		createGuestUser();

		alreadySetup = true;
	}

	@Transactional
	private void createRoleIfNotFound(String name) {
		try {
			roleRepository.findByName(name).get();
		} catch (NoSuchElementException e) {
			final Role role = new Role(name);
			roleRepository.save(role);
		}
	}

	@Transactional
	private void createAdminUser() {
		final Role adminRole = roleRepository.findByName(RoleConstants.ADMIN).get();
		final Role userRole = roleRepository.findByName(RoleConstants.USER).get();
		final Role guestRole = roleRepository.findByName(RoleConstants.GUEST).get();

		final String email = "admin@app.com";
		final String password = "adminappcom";

		final User user = new User(email, password);
		user.setRoles(Arrays.asList(adminRole, userRole, guestRole));
		userRepository.save(user);
	}

	@Transactional
	private void createGuestUser() {
		final Role guestRole = roleRepository.findByName(RoleConstants.GUEST).get();

		final String email = "guest@app.com";
		final String password = "guestappcom";

		final User user = new User(email, passwordEncoder.encode(password));
		user.setRoles(Arrays.asList(guestRole));

		userRepository.save(user);
	}

}