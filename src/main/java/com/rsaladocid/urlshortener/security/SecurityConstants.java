package com.rsaladocid.urlshortener.security;

import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpHeaders;

public class SecurityConstants {

	public static final String SECRET = "SecretKeyToGenJWTs";
	public static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(10);
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";
	public static final String HEADER_AUTHORIZATION_KEY = HttpHeaders.AUTHORIZATION;
	public static final String LOGIN_URL = "/login";

}
