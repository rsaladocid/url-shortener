package com.rsaladocid.urlshortener.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

	public JwtAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final String header = request.getHeader(SecurityConstants.HEADER_AUTHORIZATION_KEY);

		if (tokenExist(header)) {
			setAuthentication(request);
		}

		chain.doFilter(request, response);
	}

	private boolean tokenExist(String header) {
		return header != null && header.startsWith(SecurityConstants.TOKEN_BEARER_PREFIX);
	}

	private void setAuthentication(HttpServletRequest request) {
		final UsernamePasswordAuthenticationToken authenticationToken = getAuthenticationToken(request);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
	}

	private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request) {
		final String header = request.getHeader(SecurityConstants.HEADER_AUTHORIZATION_KEY);

		if (tokenExist(header)) {
			return buildAuthenticationToken(header);
		}

		return null;
	}

	private UsernamePasswordAuthenticationToken buildAuthenticationToken(String header) {
		String user = parseToken(header);

		if (user != null) {
			return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
		}

		return null;
	}

	private String parseToken(String token) {
		return Jwts.parser().setSigningKey(SecurityConstants.SECRET.getBytes())
				.parseClaimsJws(token.replace(SecurityConstants.TOKEN_BEARER_PREFIX, "")).getBody().getSubject();
	}

}
