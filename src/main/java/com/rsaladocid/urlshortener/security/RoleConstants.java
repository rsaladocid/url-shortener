package com.rsaladocid.urlshortener.security;

public class RoleConstants {

	public static final String SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
	public static final String GUEST = "ROLE_GUEST";

}
