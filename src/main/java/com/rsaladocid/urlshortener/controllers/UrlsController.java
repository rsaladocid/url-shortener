package com.rsaladocid.urlshortener.controllers;

import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.rsaladocid.urlshortener.commands.ResolveUrlCommand;
import com.rsaladocid.urlshortener.commands.ShortenUrlCommand;
import com.rsaladocid.urlshortener.dto.NullShortenedUrlDto;
import com.rsaladocid.urlshortener.dto.ShortenedUrlDto;
import com.rsaladocid.urlshortener.model.ShortenedUrl;
import com.rsaladocid.urlshortener.service.ShortenedUrlService;
import com.rsaladocid.urlshortener.service.ShorteningService;

/**
 * Manages the requests related to the {@link ShortenedUrl}s.
 */
@RestController
@RequestMapping(value = "/urls")
public class UrlsController {

	@Autowired
	private ShorteningService shorteningService;

	@Autowired
	private ShortenedUrlService shortenedUrlService;

	@Autowired
	protected URL clientBaseUrl;

	/**
	 * Redirect to the URL registered by the given short code.
	 * 
	 * @param shortCode
	 *            the short code of the URL to redirect.
	 * @return a redirection response.
	 */
	@GetMapping(value = "/{shortCode}")
	public ModelAndView resolve(@PathVariable("shortCode") String shortCode) {
		ResolveUrlCommand command = buildResolveUrlCommand(shortCode);
		ShortenedUrlDto shortenedUrl = shorteningService.resolveUrl(command);
		return buildRedirection(shortenedUrl);
	}

	private ResolveUrlCommand buildResolveUrlCommand(String shortCode) {
		return new ResolveUrlCommand(shortCode);
	}

	private ModelAndView buildRedirection(ShortenedUrlDto shortenedUrl) {
		if (shortenedUrlExist(shortenedUrl)) {
			return new ModelAndView("redirect:" + shortenedUrl.getLongUrl());
		} else {
			return new ModelAndView("redirect:" + clientBaseUrl);
		}
	}

	private boolean shortenedUrlExist(ShortenedUrlDto shortenedUrl) {
		return !(shortenedUrl instanceof NullShortenedUrlDto);
	}

	/**
	 * Register a given URL to be shortened.
	 * 
	 * @param command
	 *            the information about the URL to shorten.
	 * @return the response containing information related to the shortened URL.
	 */
	@PostMapping
	public ShortenedUrlDto create(@RequestBody ShortenUrlCommand command) {
		return shorteningService.shortenUrl(command);
	}

	/**
	 * Returns all existing shortened URLs.
	 * 
	 * @param page
	 *            the information to paginate the result.
	 * @return the page with the corresponding shortened URLs.
	 */
	@GetMapping
	public Page<ShortenedUrl> getAll(
			@PageableDefault(sort = "createdDate", direction = Sort.Direction.DESC, value = 25) Pageable page) {
		return shortenedUrlService.getAllShortenedUrls(page);
	}

}
