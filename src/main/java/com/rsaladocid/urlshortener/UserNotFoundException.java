package com.rsaladocid.urlshortener;

/**
 * Exception that can be thrown when a user is not found.
 */
public final class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1710333599933908765L;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public UserNotFoundException(final String message) {
		super(message);
	}

	public UserNotFoundException(final Throwable cause) {
		super(cause);
	}

}
