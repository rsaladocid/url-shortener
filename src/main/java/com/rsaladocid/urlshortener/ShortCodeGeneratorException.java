package com.rsaladocid.urlshortener;

import com.rsaladocid.urlshortener.generator.ShortCodeGenerator;

/**
 * Exception that can be thrown during the generation of a short code by
 * {@link ShortCodeGenerator}.
 */
public class ShortCodeGeneratorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ShortCodeGeneratorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ShortCodeGeneratorException(String message) {
		super(message);
	}

}