package com.rsaladocid.urlshortener.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.rsaladocid.urlshortener.model.ShortenedUrl;
import com.rsaladocid.urlshortener.repositories.ShortenedUrlRepository;

/**
 * Service that provides operations to manage the shortened URLs.
 */
@Service
public class ShortenedUrlService {

	@Autowired
	private ShortenedUrlRepository shortenedUrlRepository;

	/**
	 * Returns all existing shortened URLs.
	 * 
	 * @param pageRequest
	 *            the information to paginate and sort the resulting shortened URLs.
	 * @return the page containing the shortened URLs.
	 */
	public Page<ShortenedUrl> getAllShortenedUrls(Pageable pageRequest) {
		return shortenedUrlRepository.findAll(pageRequest);
	}

}
