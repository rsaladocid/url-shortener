package com.rsaladocid.urlshortener.service;

import java.util.Arrays;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.rsaladocid.urlshortener.UserAlreadyExistException;
import com.rsaladocid.urlshortener.commands.CreateUserCommand;
import com.rsaladocid.urlshortener.commands.DeleteUserCommand;
import com.rsaladocid.urlshortener.dto.UserDto;
import com.rsaladocid.urlshortener.dto.UserDtoAssembler;
import com.rsaladocid.urlshortener.model.User;
import com.rsaladocid.urlshortener.repositories.RoleRepository;
import com.rsaladocid.urlshortener.repositories.UserRepository;
import com.rsaladocid.urlshortener.security.RoleConstants;

/**
 * Service that provides operations on users.
 */
@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserDtoAssembler userDtoAssembler;

	/**
	 * Register a new administrator user.
	 * 
	 * @param createUserCommand
	 *            the information to create a new user.
	 * @return the information about the registered user.
	 */
	public UserDto createAdmin(CreateUserCommand createUserCommand) {
		final String email = createUserCommand.getEmail();
		if (emailExist(email)) {
			throw new UserAlreadyExistException("There is an admin user with that email adress: " + email);
		}

		final String password = createUserCommand.getPassword();
		final User newUser = buildAdminUser(email, password);

		userRepository.save(newUser);

		return userDtoAssembler.assemble(newUser);
	}

	private boolean emailExist(String email) {
		return userRepository.findByEmail(email) != null;
	}

	private User buildAdminUser(String email, String password) {
		final String encodedPassword = passwordEncoder.encode(password);
		final User user = new User(email, encodedPassword);
		user.setRoles(Arrays.asList(roleRepository.findByName(RoleConstants.ADMIN).get()));

		return user;
	}

	/**
	 * Removes an existing user.
	 * 
	 * @param deleteUserCommand
	 *            the information about the user to remove.
	 */
	public void deleteUser(DeleteUserCommand deleteUserCommand) {
		try {
			Long id = deleteUserCommand.getId();
			User user = userRepository.findById(id).get();
			userRepository.delete(user);
		} catch (NoSuchElementException e) {
			throw new UsernameNotFoundException("There is no such user with that id: " + deleteUserCommand.getId(), e);
		}
	}

}
