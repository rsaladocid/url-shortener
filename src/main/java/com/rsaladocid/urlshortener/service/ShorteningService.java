package com.rsaladocid.urlshortener.service;

import java.net.URL;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsaladocid.urlshortener.commands.ResolveUrlCommand;
import com.rsaladocid.urlshortener.commands.ShortenUrlCommand;
import com.rsaladocid.urlshortener.dto.NullShortenedUrlDto;
import com.rsaladocid.urlshortener.dto.ShortenedUrlDto;
import com.rsaladocid.urlshortener.dto.ShortenedUrlDtoAssembler;
import com.rsaladocid.urlshortener.generator.ShortCodeGenerator;
import com.rsaladocid.urlshortener.model.Click;
import com.rsaladocid.urlshortener.model.ShortenedUrl;
import com.rsaladocid.urlshortener.repositories.ClickRepository;
import com.rsaladocid.urlshortener.repositories.ShortenedUrlRepository;

/**
 * Service that provides operations to shorten and resolve URLs.
 */
@Service
@Transactional
public class ShorteningService {

	@Autowired
	protected URL serverBaseUrl;

	@Autowired
	private ShortenedUrlRepository shortenedUrlRepository;

	@Autowired
	private ClickRepository clickUrlRepository;

	@Autowired
	private ShortCodeGenerator shortCodeGenerator;

	@Autowired
	private ShortenedUrlDtoAssembler shortenedUrlDtoAssembler;

	/**
	 * Operation to shorten a particular URL.
	 * 
	 * @param shortenUrlCommand
	 *            the information to shorten a particular URL.
	 * @return the information about the shortened URL.
	 */
	public ShortenedUrlDto shortenUrl(ShortenUrlCommand shortenUrlCommand) {
		final URL urlToShorten = shortenUrlCommand.getUrl();
		final ShortenedUrl shortenedUrl = retrieveOrCreateShortenedUrl(urlToShorten);

		return buildShortenedUrlDto(shortenedUrl);
	}

	private ShortenedUrl retrieveOrCreateShortenedUrl(URL urlToShorten) {
		try {
			return retrieveShortenedUrlByTargetUrl(urlToShorten);
		} catch (NoSuchElementException e) {
			return createShortenedUrl(urlToShorten);
		}
	}

	private ShortenedUrl retrieveShortenedUrlByTargetUrl(URL targetUrl) {
		final Optional<ShortenedUrl> shortenedUrl = shortenedUrlRepository.findByTargetUrl(targetUrl);
		return shortenedUrl.get();
	}

	private ShortenedUrl createShortenedUrl(URL targetUrl) {
		final ShortenedUrl shortenedUrl = buildShortenedUrl(targetUrl);
		saveShortenedUrl(shortenedUrl);

		return shortenedUrl;
	}

	private ShortenedUrl buildShortenedUrl(URL targetUrl) {
		final ShortenedUrl shortenedUrl = new ShortenedUrl(targetUrl);
		fillId(shortenedUrl);
		fillShortCode(shortenedUrl);
		return shortenedUrl;
	}

	private void fillId(ShortenedUrl newShortenedUrl) {
		saveShortenedUrl(newShortenedUrl);
	}

	private void fillShortCode(ShortenedUrl savedShortUrl) {
		final Long id = savedShortUrl.getId();
		savedShortUrl.setShortCode(shortCodeGenerator.generateFrom(id));
	}

	private void saveShortenedUrl(ShortenedUrl shortUrlToSave) {
		shortenedUrlRepository.save(shortUrlToSave);
	}

	private ShortenedUrlDto buildShortenedUrlDto(ShortenedUrl shortenedUrl) {
		return shortenedUrlDtoAssembler.assemble(shortenedUrl, serverBaseUrl);
	}

	/**
	 * Operation to resolve a shortened URL.
	 * 
	 * @param resolveUrlCommand
	 *            the information to resolve a particular URL.
	 * @return the information about the shortened URL.
	 */
	public ShortenedUrlDto resolveUrl(ResolveUrlCommand resolveUrlCommand) {
		try {
			final String shortCode = resolveUrlCommand.getShortCode();
			final ShortenedUrl shortenedUrl = retrieveShortenedUrlByShortCode(shortCode);
			createClick(shortenedUrl);

			return buildShortenedUrlDto(shortenedUrl);
		} catch (NoSuchElementException e) {
			return new NullShortenedUrlDto();
		}
	}

	private ShortenedUrl retrieveShortenedUrlByShortCode(String shortCode) {
		return shortenedUrlRepository.findByShortCode(shortCode).get();
	}

	private Click createClick(ShortenedUrl shortenedUrl) {
		final Click click = new Click(shortenedUrl);
		saveClick(click);

		return click;
	}

	private void saveClick(Click clickToSave) {
		clickUrlRepository.save(clickToSave);
	}

}
