package com.rsaladocid.urlshortener.service;

import java.util.Collections;
import java.util.NoSuchElementException;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rsaladocid.urlshortener.repositories.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private UserRepository userRepository;

	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			com.rsaladocid.urlshortener.model.User user = userRepository.findByEmail(username).get();
			return new User(user.getEmail(), user.getPassword(), Collections.emptyList());
		} catch (NoSuchElementException e) {
			throw new UsernameNotFoundException(username);
		}
	}

}
