package com.rsaladocid.urlshortener;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Entry point of the URL Shortener application
 */
@SpringBootApplication
public class UrlShortenerApplication {

	@Value("${serverBaseUrl}")
	private String serverBaseUrl;

	@Value("${clientBaseUrl}")
	private String clientBaseUrl;

	@Bean
	URL serverBaseUrl() throws MalformedURLException {
		return new URL(serverBaseUrl);
	}

	@Bean
	URL clientBaseUrl() throws MalformedURLException {
		return new URL(clientBaseUrl);
	}

	public static void main(String[] args) {
		SpringApplication.run(UrlShortenerApplication.class, args);
	}

}
