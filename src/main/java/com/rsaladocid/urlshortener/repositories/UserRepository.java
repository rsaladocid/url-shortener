package com.rsaladocid.urlshortener.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.rsaladocid.urlshortener.model.User;

public interface UserRepository extends BaseRepository<User, Long> {

	Optional<User> findByEmail(String email);

	Page<User> findAll(Pageable pageable);

	void delete(User user);

}
