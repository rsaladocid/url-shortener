package com.rsaladocid.urlshortener.repositories;

import java.util.List;

import com.rsaladocid.urlshortener.model.Click;
import com.rsaladocid.urlshortener.model.ShortenedUrl;

public interface ClickRepository extends BaseRepository<Click, Long> {

	List<Click> findByShortenedUrl(ShortenedUrl shortenedUrl);

}
