package com.rsaladocid.urlshortener.repositories;

import java.util.Optional;

import com.rsaladocid.urlshortener.model.Role;

public interface RoleRepository extends BaseRepository<Role, Long> {

	Optional<Role> findByName(String name);

}
