package com.rsaladocid.urlshortener.repositories;

import java.net.URL;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.rsaladocid.urlshortener.model.ShortenedUrl;

public interface ShortenedUrlRepository extends BaseRepository<ShortenedUrl, Long> {

	Optional<ShortenedUrl> findByShortCode(String shortCode);

	Optional<ShortenedUrl> findByTargetUrl(URL targetUrl);

	Page<ShortenedUrl> findAll(Pageable pageable);

}
