package com.rsaladocid.urlshortener.model;

import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "urls")
public class ShortenedUrl {

	@Id
	@GeneratedValue
	private Long id;

	private String shortCode;

	@Column(nullable = false)
	private URL targetUrl;

	@Column(nullable = false)
	private Date createdDate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "shortenedUrl")
	private Collection<Click> clicks;

	public ShortenedUrl(URL targetUrl) {
		this(targetUrl, new Date());
	}

	public ShortenedUrl(URL targetUrl, Date createdDate) {
		this.targetUrl = targetUrl;
		this.createdDate = createdDate;
		clicks = new HashSet<Click>();
	}

	protected ShortenedUrl() {
	}

	public Long getId() {
		return id;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public URL getTargetUrl() {
		return targetUrl;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Collection<Click> getClicks() {
		return clicks;
	}

}
