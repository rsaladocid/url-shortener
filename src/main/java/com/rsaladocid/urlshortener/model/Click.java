package com.rsaladocid.urlshortener.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "clicks")
public class Click {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private Date createdDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "url_id", nullable = false)
	private ShortenedUrl shortenedUrl;

	public Click(ShortenedUrl shortenedUrl) {
		this(shortenedUrl, new Date());
	}

	public Click(ShortenedUrl shortenedUrl, Date createdDate) {
		this.shortenedUrl = shortenedUrl;
		this.createdDate = createdDate;
	}

	protected Click() {
	}

	public Long getId() {
		return id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public ShortenedUrl getShortenedUrl() {
		return shortenedUrl;
	}

}
