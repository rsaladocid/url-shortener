package com.rsaladocid.urlshortener.generator;

import org.springframework.stereotype.Component;

import com.rsaladocid.urlshortener.ShortCodeGeneratorException;
import com.rsaladocid.urlshortener.util.Base62;

/**
 * Generator of short codes converting to base 62 strings.
 */
@Component
public class Base62IdCodeGenerator implements ShortCodeGenerator {

	/**
	 * Creates a base 62 short code.
	 * 
	 * @param id
	 *            the number to convert to base 62.
	 */
	public String generateFrom(Long id) {
		try {
			return createBase62IdFromBase10Id(id);
		} catch (NullPointerException e) {
			throw new ShortCodeGeneratorException("Id does not exist", e);
		}
	}

	private String createBase62IdFromBase10Id(long id) {
		return Base62.fromBase10(id);
	}

}
