package com.rsaladocid.urlshortener.generator;

/**
 * Generator of sort codes.
 */
public interface ShortCodeGenerator {

	/**
	 * Creates a short code from a given number.
	 * 
	 * @param number
	 *            the number used to generate a short code.
	 * @return the short code.
	 */
	public String generateFrom(Long number);

}
