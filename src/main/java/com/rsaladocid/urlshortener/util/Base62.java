package com.rsaladocid.urlshortener.util;

import com.rsaladocid.urlshortener.Base62ConverterException;

/**
 * This class provides methods to convert to/from a base 62 string.
 */
public class Base62 {

	/**
	 * The letters and symbols in a fixed order used to represent a base 62 string.
	 */
	public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * The total number of letters and numbers used by the alphabet.
	 */
	public static final int BASE = ALPHABET.length();

	private Base62() {
	}

	/**
	 * Turns a base 10 number into a corresponding base 62 string.
	 * 
	 * @param number
	 *            the base 10 number to convert.
	 * @return the base 62 string.
	 */
	public static String fromBase10(long number) {
		return convertNumberToString(number);
	}

	private static String convertNumberToString(long number) {
		String string = "";
		long remaining = number;

		while (remaining > 0) {
			char letter = convertNumberToLetter(remaining);
			string += letter;
			remaining /= BASE;
		}

		return createReverseString(string.toString());
	}

	private static char convertNumberToLetter(long number) {
		return ALPHABET.charAt((int) (number % BASE));
	}

	private static String createReverseString(String string) {
		return new StringBuilder(string).reverse().toString();
	}

	/**
	 * Turns a base 62 string into a base 10 number.
	 * 
	 * @param string
	 *            the base 62 string to convert.
	 * @return the base 10 number.
	 */
	public static long toBase10(String string) {
		throwExceptionIfStringIsEmpty(string);
		String reverseString = createReverseString(string);
		return convertStringToNumber(reverseString);
	}

	private static void throwExceptionIfStringIsEmpty(String string) {
		if (string.isEmpty()) {
			throw new Base62ConverterException("String cannot be empty");
		}
	}

	private static long convertStringToNumber(String string) {
		long number = 0;

		for (int i = string.length() - 1; i >= 0; i--) {
			throwExceptionIfLetterIsIllegal(string.charAt(i));
			number += convertLetterToNumber(string.charAt(i), i);
		}

		return number;
	}

	private static void throwExceptionIfLetterIsIllegal(char letter) {
		if (ALPHABET.indexOf(letter) < 0) {
			throw new Base62ConverterException("Illegal letter: " + letter);
		}
	}

	private static long convertLetterToNumber(char letter, int positionInTheString) {
		int positionInTheAlphabet = ALPHABET.indexOf(letter);
		return positionInTheAlphabet * (long) Math.pow(BASE, positionInTheString);
	}

}
