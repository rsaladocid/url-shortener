package com.rsaladocid.urlshortener.commands;

/**
 * Command containing the information to remove a {@link User}.
 */
public class DeleteUserCommand {

	private Long id;

	public DeleteUserCommand(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

}
