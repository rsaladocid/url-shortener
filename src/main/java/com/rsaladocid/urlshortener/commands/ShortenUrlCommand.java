package com.rsaladocid.urlshortener.commands;

import java.net.URL;

import com.rsaladocid.urlshortener.model.ShortenedUrl;

/**
 * Command containing the information to create a {@link ShortenedUrl}.
 */
public class ShortenUrlCommand {

	private URL url;

	public ShortenUrlCommand(URL url) {
		this.url = url;
	}

	protected ShortenUrlCommand() {

	}

	public URL getUrl() {
		return url;
	}

}
