package com.rsaladocid.urlshortener.commands;

/**
 * Command containing information to resolve a {@link ShortenedUrl}.
 */
public class ResolveUrlCommand {

	private String shortChode;

	public ResolveUrlCommand(String shortChode) {
		this.shortChode = shortChode;
	}

	protected ResolveUrlCommand() {

	}

	public String getShortCode() {
		return shortChode;
	}

}
