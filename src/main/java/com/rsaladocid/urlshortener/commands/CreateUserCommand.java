package com.rsaladocid.urlshortener.commands;

/**
 * Command containing the information to create a {@link User}.
 */
public class CreateUserCommand {

	private String email;
	private String password;

	public CreateUserCommand(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

}
