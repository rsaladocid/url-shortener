package com.rsaladocid.urlshortener.dto;

public class NullShortenedUrlDto extends ShortenedUrlDto {

	public NullShortenedUrlDto() {
		setCreatedDate(null);
		setLongUrl(null);
		setShortUrl(null);
		setShortCode(null);
		setNumberOfClicks(-1);
	}

}
