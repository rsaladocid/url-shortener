package com.rsaladocid.urlshortener.dto;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Component;

import com.rsaladocid.urlshortener.model.ShortenedUrl;

@Component
public class ShortenedUrlDtoAssembler {

	public ShortenedUrlDto assemble(ShortenedUrl shortenedUrl, URL baseUrl) {
		ShortenedUrlDto shortenedUrlDto = new ShortenedUrlDto();

		shortenedUrlDto.setShortUrl(buildShortUrl(baseUrl, shortenedUrl.getShortCode()));
		shortenedUrlDto.setLongUrl(shortenedUrl.getTargetUrl());
		shortenedUrlDto.setShortCode(shortenedUrl.getShortCode());
		shortenedUrlDto.setCreatedDate(shortenedUrl.getCreatedDate());
		shortenedUrlDto.setNumberOfClicks(shortenedUrl.getClicks().size());

		return shortenedUrlDto;
	}

	private URL buildShortUrl(URL baseUrl, String shortChode) {
		try {
			return new URL(baseUrl.getProtocol(), baseUrl.getHost(), baseUrl.getPort(), "/" + shortChode);
		} catch (MalformedURLException e) {
			throw new RuntimeException("Invalid base URL", e);
		}
	}

}
