package com.rsaladocid.urlshortener.dto;

import org.springframework.stereotype.Component;

import com.rsaladocid.urlshortener.model.User;

@Component
public class UserDtoAssembler {

	public UserDto assemble(User user) {
		UserDto userDto = new UserDto();

		userDto.setId(user.getId());
		userDto.setEmail(user.getEmail());

		return userDto;
	}

}
