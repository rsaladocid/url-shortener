package com.rsaladocid.urlshortener.dto;

import java.net.URL;
import java.util.Date;

public class ShortenedUrlDto {

	private URL shortUrl;

	private URL longUrl;

	private String shortCode;

	private Date createdDate;

	private int numberOfClicks;

	public ShortenedUrlDto() {
		numberOfClicks = 0;
	}

	public URL getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(URL shortUrl) {
		this.shortUrl = shortUrl;
	}

	public URL getLongUrl() {
		return longUrl;
	}

	public void setLongUrl(URL longUrl) {
		this.longUrl = longUrl;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getNumberOfClicks() {
		return numberOfClicks;
	}

	public void setNumberOfClicks(int numberOfClicks) {
		this.numberOfClicks = numberOfClicks;
	}

}
