package com.rsaladocid.urlshortener;

import com.rsaladocid.urlshortener.util.Base62;

/**
 * Exception that can be thrown during the conversion from/to {@link Base62}.
 */
public class Base62ConverterException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public Base62ConverterException(String message, Throwable cause) {
		super(message, cause);
	}

	public Base62ConverterException(String message) {
		super(message);
	}

}