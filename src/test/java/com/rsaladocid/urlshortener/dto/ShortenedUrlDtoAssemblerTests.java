package com.rsaladocid.urlshortener.dto;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.rsaladocid.urlshortener.model.ShortenedUrl;

import static org.mockito.Mockito.when;

public class ShortenedUrlDtoAssemblerTests {

	@Mock
	private ShortenedUrl shortenedUrl;

	private ShortenedUrlDtoAssembler shortenedUrlDtoAssembler;

	@Before
	public void setUp() {
		shortenedUrlDtoAssembler = new ShortenedUrlDtoAssembler();
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = NullPointerException.class)
	public void whenAssembleWithNullShortenedUrl_ThenThrowException() throws MalformedURLException {
		shortenedUrlDtoAssembler.assemble(null, new URL("http://www.google.es"));
	}

	@Test(expected = NullPointerException.class)
	public void whenAssembleWithNullBaseUrl_ThenThrowException() throws MalformedURLException {
		shortenedUrlDtoAssembler.assemble(shortenedUrl, null);
	}

	@Test
	public void whenAssemble_ThenReturnDtoContainingTheCorrespondingShortUrl() throws MalformedURLException {
		when(shortenedUrl.getShortCode()).thenReturn("1ca");
		URL baseUrl = new URL("http://www.google.es");

		ShortenedUrlDto shortedUrlDto = shortenedUrlDtoAssembler.assemble(shortenedUrl, baseUrl);
		assertTrue(shortedUrlDto.getShortUrl().equals(new URL(baseUrl, "1ca")));
	}

}
