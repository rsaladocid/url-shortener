package com.rsaladocid.urlshortener.service;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.rsaladocid.urlshortener.commands.ResolveUrlCommand;
import com.rsaladocid.urlshortener.commands.ShortenUrlCommand;
import com.rsaladocid.urlshortener.dto.NullShortenedUrlDto;
import com.rsaladocid.urlshortener.dto.ShortenedUrlDto;
import com.rsaladocid.urlshortener.dto.ShortenedUrlDtoAssembler;
import com.rsaladocid.urlshortener.generator.ShortCodeGenerator;
import com.rsaladocid.urlshortener.model.ShortenedUrl;
import com.rsaladocid.urlshortener.repositories.ClickRepository;
import com.rsaladocid.urlshortener.repositories.ShortenedUrlRepository;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShorteningServiceTests {

	@Mock
	private ShortenedUrlRepository shortenedUrlRepository;
	
	@Mock
	private ClickRepository clickRepository;

	@Mock
	private ShortCodeGenerator shortCodeGenerator;

	@Mock
	private ShortenedUrlDtoAssembler shortenedUrlDtoAssembler;

	@InjectMocks
	private ShorteningService service;

	private URL baseUrl;

	@Before
	public void setUp() throws Exception {
		service = new ShorteningService();
		baseUrl = new URL("http://oso.co/");
		service.serverBaseUrl = baseUrl;
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void whenShortenedUrlAndDoesNotExist_ThenReturnShortenedUrlDto() throws MalformedURLException {
		URL urlToShorten = new URL("http://www.google.com");
		String shortCode = "1aa";

		when(shortenedUrlRepository.findByTargetUrl(urlToShorten)).thenReturn(Optional.ofNullable(null));
		when(shortCodeGenerator.generateFrom(Mockito.any())).thenReturn(shortCode);
		when(shortenedUrlDtoAssembler.assemble(Mockito.any(), Mockito.any())).thenCallRealMethod();

		ShortenUrlCommand command = new ShortenUrlCommand(urlToShorten);
		ShortenedUrlDto shortedUrlDto = service.shortenUrl(command);

		assertNotNull(shortedUrlDto);
		assertTrue(shortedUrlDto.getLongUrl().equals(urlToShorten));
		assertTrue(shortedUrlDto.getShortUrl().equals(new URL(baseUrl, shortCode)));
		assertTrue(shortedUrlDto.getShortCode().equals(shortCode));
		assertTrue(shortedUrlDto.getCreatedDate() != null);
	}

	@Test
	public void whenShortenUrlAndAlreadyExist_ThenReturnShortenedUrlDto() throws MalformedURLException {
		URL urlToShorten = new URL("http://www.google.com");
		String shortCode = "1aa";

		ShortenedUrl shortedUrl = new ShortenedUrl(urlToShorten, new Date());
		shortedUrl.setShortCode(shortCode);

		when(shortenedUrlRepository.findByTargetUrl(urlToShorten)).thenReturn(Optional.of(shortedUrl));
		when(shortenedUrlDtoAssembler.assemble(Mockito.any(), Mockito.any())).thenCallRealMethod();

		ShortenUrlCommand command = new ShortenUrlCommand(urlToShorten);
		ShortenedUrlDto shortedUrlDto = service.shortenUrl(command);

		assertNotNull(shortedUrlDto);
		assertTrue(shortedUrlDto.getLongUrl().equals(urlToShorten));
		assertTrue(shortedUrlDto.getShortUrl().equals(new URL(baseUrl, shortCode)));
		assertTrue(shortedUrlDto.getShortCode().equals(shortCode));
		assertTrue(shortedUrlDto.getCreatedDate() != null);
	}

	@Test
	public void whenResolveExistingShortenedUrl_ThenReturnShortenedUrlDto() throws MalformedURLException {
		URL urlToShorten = new URL("http://www.google.com");
		String shortCode = "1aa";

		ShortenedUrl shortedUrl = new ShortenedUrl(urlToShorten, new Date());
		shortedUrl.setShortCode(shortCode);

		when(shortenedUrlRepository.findByShortCode(shortCode)).thenReturn(Optional.of(shortedUrl));
		when(shortenedUrlDtoAssembler.assemble(Mockito.any(), Mockito.any())).thenCallRealMethod();
		when(clickRepository.findByShortenedUrl(shortedUrl)).thenReturn(Lists.emptyList());
		
		ResolveUrlCommand command = new ResolveUrlCommand(shortCode);
		ShortenedUrlDto shortedUrlDto = service.resolveUrl(command);

		assertNotNull(shortedUrlDto);
		assertTrue(shortedUrlDto.getLongUrl().equals(urlToShorten));
		assertTrue(shortedUrlDto.getShortUrl().equals(new URL(baseUrl, shortCode)));
		assertTrue(shortedUrlDto.getShortCode().equals(shortCode));
		assertTrue(shortedUrlDto.getCreatedDate() != null);
		assertTrue(shortedUrlDto.getNumberOfClicks() == 0);
	}

	@Test
	public void whenResolveUnexistingShortenedUrl_ThenReturnShortenedUrlDto() throws MalformedURLException {
		String shortCode = "1aa";

		when(shortenedUrlRepository.findByShortCode(shortCode)).thenReturn(Optional.ofNullable(null));

		ResolveUrlCommand command = new ResolveUrlCommand(shortCode);
		ShortenedUrlDto shortedUrlDto = service.resolveUrl(command);

		assertTrue(shortedUrlDto instanceof NullShortenedUrlDto);
	}

}
