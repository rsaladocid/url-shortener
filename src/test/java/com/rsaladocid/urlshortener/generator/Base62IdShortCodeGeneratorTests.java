package com.rsaladocid.urlshortener.generator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.rsaladocid.urlshortener.ShortCodeGeneratorException;
import com.rsaladocid.urlshortener.model.ShortenedUrl;

import static org.mockito.Mockito.when;

public class Base62IdShortCodeGeneratorTests {

	@Mock
	private ShortenedUrl shortenedUrl;

	private Base62IdCodeGenerator generator;

	@Before
	public void setUp() {
		generator = new Base62IdCodeGenerator();
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = ShortCodeGeneratorException.class)
	public void whenGenerateFromNull_ThenThrowException() {
		generator.generateFrom(null);
	}

	@Test(expected = ShortCodeGeneratorException.class)
	public void whenGenerateFromShortenedUrlWithoutId_thenThrowException() {
		when(shortenedUrl.getId()).thenReturn(null);

		generator.generateFrom(shortenedUrl.getId());
	}

	@Test
	public void whenGenerateFromShortenedUrlWithId_thenReturnShortCode() {
		when(shortenedUrl.getId()).thenReturn((long) 1);
		String shortCode = generator.generateFrom(shortenedUrl.getId());

		assertNotNull(shortCode);
	}

}
