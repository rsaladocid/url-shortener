package com.rsaladocid.urlshortener.util;

import static org.junit.Assert.*;

import org.junit.Test;

import com.rsaladocid.urlshortener.Base62ConverterException;

public class Base62Tests {

	@Test
	public void whenConvertANegativeNumber_ThenReturnEmptyString() {
		String result = Base62.fromBase10(-1);
		assertTrue(result.isEmpty());
	}

	@Test
	public void whenConvertABase10PositiveNumber_ThenReturnCorrespondingBase64String() {
		String result = Base62.fromBase10(125);
		assertTrue(result.equals("cb"));
	}

	@Test
	public void whenConvertABase62String_ThenReturnCorrespondingBase10Number() {
		long result = Base62.toBase10("cb");
		assertTrue(result == 125);
	}

	@Test(expected = Base62ConverterException.class)
	public void whenConvertABase62StringContainingIllegalLetters_ThenThrowException() {
		Base62.toBase10("cb@");
	}

	@Test(expected = Base62ConverterException.class)
	public void whenConvertAnEmptyString_ThenThrowException() {
		Base62.toBase10("");
	}

	@Test(expected = NullPointerException.class)
	public void whenConvertANullString_ThenThrowException() {
		Base62.toBase10(null);
	}

}
